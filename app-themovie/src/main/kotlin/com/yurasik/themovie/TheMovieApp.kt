package com.yurasik.themovie

import android.app.Application
import com.yurasik.themovie.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import com.yurasik.themovie.core.di.injector.AppInjector
import yurasik.com.themovie.BuildConfig
import javax.inject.Inject


class TheMovieApp @Inject constructor(): Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        // else Timber.plant(CrashReportingTree())

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)

        AppInjector.init(this)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}