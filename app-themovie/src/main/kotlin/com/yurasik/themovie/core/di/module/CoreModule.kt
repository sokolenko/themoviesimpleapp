package com.yurasik.themovie.core.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import retrofit2.converter.gson.GsonConverterFactory
import com.yurasik.data.executor.RemoteExecutor
import com.yurasik.domain.executor.IExecutor
import javax.inject.Singleton

/**
 * Dagger module to provide core functionality.
 */

@Suppress("unused")
@Module
class CoreModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
            GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideRemoteExecutor(): IExecutor =
        RemoteExecutor()
}
