package com.yurasik.themovie.core.di.module

import com.yurasik.domain.repositories.IMoviesPaginatedRepository
import com.yurasik.domain.usecase.movies.MoviesPaginatedUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Suppress("unused")
@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideMoviesUseCase(remoteMoviesRepository: IMoviesPaginatedRepository)
            : MoviesPaginatedUseCase {
        return MoviesPaginatedUseCase(remoteMoviesRepository)
    }

}