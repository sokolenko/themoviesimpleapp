package com.yurasik.themovie.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.yurasik.themovie.core.di.viewmodule.ViewModelModule
import com.yurasik.themovie.presentation.ui.MainActivity

@Suppress("unused")
@Module(includes = [
    ViewModelModule::class,
    UseCaseModule::class,
    Repository::class,
    DatasourceModule::class
    ])
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class])
    abstract fun bindingMainActivity() : MainActivity
}