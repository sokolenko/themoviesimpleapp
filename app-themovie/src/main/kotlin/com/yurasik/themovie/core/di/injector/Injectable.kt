package com.yurasik.themovie.core.di.injector

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable