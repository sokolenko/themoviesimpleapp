package com.yurasik.themovie.core.di.module

import javax.inject.Qualifier

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class Movies

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class ScropeIO

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class Remote

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class Cache
