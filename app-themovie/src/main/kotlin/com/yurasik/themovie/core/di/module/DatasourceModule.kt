package com.yurasik.themovie.core.di.module

import dagger.Module
import dagger.Provides
import com.yurasik.data.datasource.movies.IMoviesPaginatedDataSource
import com.yurasik.data.remote.datasource.MoviesPaginatedRemoteDataSource
import com.yurasik.data.remote.retrofit.movie.api.MoviesApiService
import com.yurasik.data.remote.retrofit.movie.mapper.MovieMapper
import com.yurasik.domain.executor.IExecutor
import javax.inject.Singleton

/*
* Dagger module to provide datasources.
*/

@Suppress("unused")
@Module(includes = [NetworkModule::class])
class DatasourceModule {

    @Provides
    @Singleton
    //@Remote
    fun provideRemotePaginatedMoviesDataSource(
        remoteExecutor: IExecutor,
        moviesApiService: MoviesApiService
    ): IMoviesPaginatedDataSource {
        return MoviesPaginatedRemoteDataSource(
            remoteExecutor,
            MovieMapper(),
            moviesApiService
        )
    }

}