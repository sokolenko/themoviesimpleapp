package com.yurasik.themovie.core.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import com.yurasik.data.remote.retrofit.createOkHttpClient
import com.yurasik.data.remote.retrofit.createRetrofit
import com.yurasik.data.remote.retrofit.createWebService
import com.yurasik.data.remote.retrofit.interceptor.AccessKeyPathInterceptor
import com.yurasik.data.remote.retrofit.movie.api.MoviesApiService
import com.yurasik.domain.util.rx.retry.RetryManager
import com.yurasik.themovie.presentation.rx.DelayRetryManager
import com.yurasik.themovie.presentation.rx.InternetObserveManager
import yurasik.com.themovie.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton


@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused")
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideRetrofit(okHttpClient: OkHttpClient, gsonFactory: GsonConverterFactory): Retrofit {
        return createRetrofit(
            okHttpClient = okHttpClient,
            url = "https://api.themoviedb.org/",
            factory = gsonFactory
        )
    }

    @Provides
    @Singleton
    internal fun provideHttpClient(): OkHttpClient {
        return createOkHttpClient(
            AccessKeyPathInterceptor(),
            BuildConfig.DEBUG
        )
    }

    @Provides
    @Singleton
    internal fun provideMoviesApi(retrofit: Retrofit)=
        createWebService<MoviesApiService>(retrofit)

    @Provides
    @Singleton
    internal fun provideRetryManager(context: Application): RetryManager {
        return InternetObserveManager(context)
    }

    /*
    @Provides
    @Singleton
    internal fun provideRetryManager(context: Application): RetryManager {
        Timber.d("Context provide: $context")
        return DelayRetryManager(10,5000)
    }
     */
}