package com.yurasik.themovie.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.yurasik.themovie.presentation.ui.MovieFragment

@Suppress("unused")
@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMovieFragment(): MovieFragment

}