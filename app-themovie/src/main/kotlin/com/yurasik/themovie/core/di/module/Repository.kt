package com.yurasik.themovie.core.di.module

import dagger.Module
import dagger.Provides
import com.yurasik.data.datasource.movies.IMoviesPaginatedDataSource
import com.yurasik.data.repositories.MoviesRemoteRepository
import com.yurasik.domain.repositories.IMoviesPaginatedRepository
import javax.inject.Singleton

@Suppress("unused")
@Module
class Repository {

    @Provides
    @Singleton
    fun provideMovieRepository(remoteMoviesDataSource: IMoviesPaginatedDataSource)
            : IMoviesPaginatedRepository {
        return MoviesRemoteRepository(
            remoteMoviesDataSource
        )
    }

}