package com.yurasik.themovie.binding

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yurasik.themovie.presentation.adapter.decorators.GridSpacingItemDecoration
import com.yurasik.themovie.presentation.adapter.decorators.SpaceItemDecoration

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visibilityData: MutableLiveData<Boolean>?) {
    visibilityData?.value?.let {visible ->
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

/**
 * @param recyclerView  RecyclerView to bind to SpaceItemDecoration
 * @param spaceInPx space in dp
 */
@BindingAdapter("linearSpaceItemDecoration")
fun setLinearSpaceItemDecoration(recyclerView: RecyclerView, spaceInDp: Float) {
    if (spaceInDp > 0) {
        val itemDecoration =
            SpaceItemDecoration(
                spaceInDp.toInt(),
                true,
                false
            )
        recyclerView.addItemDecoration(itemDecoration)
    }
}

@BindingAdapter("gridSpaceItemDecoration")
fun setGridSpaceItemDecoration(recyclerView: RecyclerView, spaceInDp: Float) {
    if (spaceInDp > 0) {
        val grid = recyclerView.layoutManager as GridLayoutManager
        val spanCount = grid.spanCount
        val itemDecoration =
            GridSpacingItemDecoration(
                spacing = spaceInDp.toInt(), spanCount = spanCount
            )
        recyclerView.addItemDecoration(itemDecoration)
    }
}