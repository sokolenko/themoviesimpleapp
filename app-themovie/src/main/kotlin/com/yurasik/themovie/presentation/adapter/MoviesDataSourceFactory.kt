package com.yurasik.themovie.presentation.adapter

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.usecase.movies.MoviesPaginatedUseCase
import com.yurasik.domain.util.rx.retry.RetryManager
import io.reactivex.disposables.CompositeDisposable

class MoviesDataSourceFactory(
    private val moviesPaginatedUseCase: MoviesPaginatedUseCase,
    private val initialLoadingLiveData: MutableLiveData<Boolean>,
    private val noConnectionLiveData: MutableLiveData<Boolean>,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    private val retryManager: RetryManager
) : DataSource.Factory<Int, Movie>(){

    var pageMovieDataSource: PageMovieDataSource? = null

    override fun create(): DataSource<Int, Movie> {
        pageMovieDataSource =
            PageMovieDataSource(
                moviesPaginatedUseCase,
                initialLoadingLiveData,
                noConnectionLiveData,
                compositeDisposable,
                retryManager
            )
        return pageMovieDataSource!!
    }

}