package com.yurasik.themovie.presentation.adapter.viewholders

import android.view.View
import com.yurasik.domain.model.movie.Movie
import com.yurasik.themovie.presentation.adapter._base.LifecycleViewHolder

abstract class BaseMovieViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
    protected var movie: Movie? = null

    open fun bindTo(position: Int, movie: Movie?) {
        this.movie = movie
    }
}