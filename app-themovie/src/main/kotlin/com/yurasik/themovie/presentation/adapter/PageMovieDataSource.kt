package com.yurasik.themovie.presentation.adapter

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.usecase.movies.MoviesPaginatedUseCase
import com.yurasik.domain.util.rx.retry.RetryManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

class PageMovieDataSource(
    private val moviesPaginatedUseCase: MoviesPaginatedUseCase,
    private val initialLoadingLiveData: MutableLiveData<Boolean>,
    private val noConnectionLiveData: MutableLiveData<Boolean>,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    private val retryManager: RetryManager
) :
    PageKeyedDataSource<Int, Movie>() {

    private var pageCounter = 1

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movie>
    ) {
        compositeDisposable +=
        createObservable(pageCounter, params.requestedLoadSize, initialCallback = callback)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        compositeDisposable +=
        createObservable(params.key,
            params.requestedLoadSize,
            isAfter = true,
            callback = callback)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        compositeDisposable+=
        createObservable(params.key,
            params.requestedLoadSize,
            isAfter = false,
            callback = callback)
    }

    private fun createObservable(
        requestedPage: Int,
        perPage: Int,
        isAfter: Boolean = true,
        initialCallback: LoadInitialCallback<Int, Movie>? = null,
        callback: LoadCallback<Int, Movie>? = null
    ) =
            moviesPaginatedUseCase.buildUseCaseSingle(
                MoviesPaginatedUseCase.Params(requestedPage, perPage)
            )
                .doOnSubscribe {
                    initialLoadingLiveData.postValue(true)
                }
                .doOnSuccess {
                    initialLoadingLiveData.postValue(false)
                }
                .doOnError {
                    noConnectionLiveData.postValue(true)
                }
                .retryWhen { retryHandler -> retryHandler.flatMap { error -> retryManager.observeRetries(error) } }
                .doOnSuccess {
                    noConnectionLiveData.postValue(false)
                }
                .subscribe({ response ->

                    val previousPageKey = if (pageCounter==1) {
                        null
                    } else {
                        pageCounter
                    }

                    //if (response.isEmpty()) mutableProgress.value = false
                    initialCallback?.apply {
                        onResult(response, previousPageKey, pageCounter++)
                    }
                    //initialCallback?.onResult(response, previousPageKey, pageCounter++)
                    callback?.apply {
                        if (isAfter) {
                            pageCounter++
                        } else {
                            pageCounter--
                        }
                        onResult(response, pageCounter)
                    }
                }, { error ->
                    // TODO
                    Timber.e(error, "Fail on fetching page of events")
                }
                )
}