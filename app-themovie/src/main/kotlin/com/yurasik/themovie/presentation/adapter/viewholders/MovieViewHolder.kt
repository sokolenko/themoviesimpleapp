package com.yurasik.themovie.presentation.adapter.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.yurasik.app_core.livedata.SingleLiveEvent
import com.yurasik.domain.model.movie.Movie
import com.yurasik.themovie.binding.GlideApp
import yurasik.com.themovie.R

/** Using synthetic method instead of annoying method findViewById **/
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieViewHolder(
    parent: ViewGroup,
    private val moviesSelectionDelegate: SingleLiveEvent<Movie>? = null
) : BaseMovieViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)) {

    private val imageSize: Pair<Int, Int> by lazy {
        val width = parent.context.resources.getDimensionPixelSize(R.dimen.image_width)
        val heigth = parent.context.resources.getDimensionPixelSize(R.dimen.image_height)
        Pair(width, heigth)
    }

    private val circularProgressDrawable : CircularProgressDrawable by lazy {
        CircularProgressDrawable(itemView.context).apply {
            setStyle(CircularProgressDrawable.LARGE)
            setColorSchemeColors(
                ContextCompat.getColor(itemView.context, R.color.refresh_progress_3)
            )
            start()
        }
    }

    /**
     * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
     * ViewHolder when Item is loaded.
     */
    override fun bindTo(position: Int, movie: Movie?) {
        super.bindTo(position, movie)
        itemView.tvId.text = (position+1).toString()
        itemView.tvTitle.text = movie?.title
        itemView.setOnClickListener {
            moviesSelectionDelegate?.postValue(movie)
        }

        if (movie?.backdropPath == null) {
            GlideApp.with(itemView)
                .load(R.drawable.noimage)
                .into(itemView.img)
        } else {
            GlideApp.with(itemView)
                .load("https://image.tmdb.org/t/p/w185${movie.backdropPath}")
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE)
                    .error(R.drawable.noimage))
                .placeholder(circularProgressDrawable)
                .fitCenter()
                .dontAnimate()
                .override(imageSize.first, imageSize.second)
                .into(itemView.img)
        }
    }



}