package com.yurasik.themovie.presentation.ui

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.yurasik.themovie.core.di.injector.Injectable
import com.yurasik.themovie.core.di.injector.injectViewModel
import com.yurasik.themovie.presentation.adapter.MoviesAdapter
import com.yurasik.themovie.presentation.common.PAGE_SIZE
import kotlinx.android.synthetic.main.fragment_movies.*
import timber.log.Timber
import yurasik.com.themovie.R
import yurasik.com.themovie.databinding.FragmentMoviesBinding
import javax.inject.Inject

class MovieFragment : Fragment(),
    Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MoviesViewModule

    lateinit var binding: FragmentMoviesBinding
    private var snackbar: Snackbar? = null

    val moviesAdapter: MoviesAdapter by lazy {
        MoviesAdapter(viewModel.payoutSelectionDelegate)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        binding.viewModel = viewModel
        binding.adapter = moviesAdapter

        moviesAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (positionStart == 0 && itemCount<= PAGE_SIZE) {
                    recyclerView.layoutManager?.scrollToPosition(0)
                }
            }
        })

        // Observes
        viewModel.moviesLiveData.observe(viewLifecycleOwner, Observer { movies ->
            moviesAdapter.submitList(movies)
        })

        viewModel.payoutSelectionDelegate.observe(viewLifecycleOwner, Observer { movie ->
            Timber.d("Click event: $movie")
        })

        viewModel.totalPagesLiveData.observe(viewLifecycleOwner, Observer {
            activity?.title = "Total pages count: $it"
        })

        viewModel.navigatePayoutDetailsEvent.observe(viewLifecycleOwner, Observer {
            Timber.d("Navigate event: $it")
        })

        viewModel.noConnectionLiveData.observe(viewLifecycleOwner, Observer {
            // find the Content Layout id
            val contextView =
                activity?.window?.decorView?.findViewById<View>(android.R.id.content)
            if (it) {
                // Make and display Snackbar
                snackbar = Snackbar.make(contextView!!, getString(R.string.connection_error), Snackbar.LENGTH_INDEFINITE)
                snackbar?.show()
            } else {
                snackbar?.let {
                    it.dismiss()
                    snackbar = null
                    Snackbar.make(contextView!!, getString(R.string.connection_sstablished), Snackbar.LENGTH_SHORT)
                }
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_movies, container, false)
        binding.lifecycleOwner = this
       return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout.setColorSchemeResources(
            R.color.refresh_progress_1,
            R.color.refresh_progress_2)
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
            swipeRefreshLayout.isRefreshing = false
        }
    }
}