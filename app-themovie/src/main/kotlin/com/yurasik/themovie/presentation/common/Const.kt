package com.yurasik.themovie.presentation.common

const val PAGE_SIZE = 20
const val PREVIEW_IMAGE_PATH = "https://image.tmdb.org/t/p/w300"