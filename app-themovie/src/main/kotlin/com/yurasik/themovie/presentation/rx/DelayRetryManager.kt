package com.yurasik.themovie.presentation.rx

import com.yurasik.domain.util.rx.retry.RetryEvent
import com.yurasik.domain.util.rx.retry.RetryManager
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DelayRetryManager @Inject constructor(
    val maxRetries: Int,
    val retryDelayMillis: Int
) : RetryManager {

    companion object {
        val TAG = DelayRetryManager::class.java.name
    }

    private var retryCount: Int = 0

    override fun observeRetries(error: Throwable): Flowable<RetryEvent> {
        return if (++retryCount < maxRetries) {
            Flowable.timer(retryDelayMillis.toLong(), TimeUnit.MILLISECONDS).map { timer -> RetryEvent() }
        } else {
            Flowable.error(error)
        }
    }

    override fun clear() {
        retryCount = 0
    }
}