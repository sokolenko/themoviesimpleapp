package com.yurasik.themovie.presentation.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import com.yurasik.app_core.livedata.SingleLiveEvent
import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.usecase.movies.MoviesPaginatedUseCase
import com.yurasik.domain.util.rx.retry.RetryManager
import com.yurasik.themovie.presentation.adapter.MoviesDataSourceFactory
import com.yurasik.themovie.presentation.common.PAGE_SIZE
import javax.inject.Inject

class MoviesViewModule @Inject constructor(
    val moviesPaginatedUseCase: MoviesPaginatedUseCase,
    val retryManager: RetryManager
) : ViewModel() {

    val moviesLiveData: LiveData<PagedList<Movie>>
    val totalPagesLiveData = MutableLiveData<Int>()

    val noConnectionLiveData = MutableLiveData<Boolean>()

    val payoutSelectionDelegate = SingleLiveEvent<Movie>()
    val navigatePayoutDetailsEvent = payoutSelectionDelegate

    private val compositeDisposable = CompositeDisposable()

    val loadingVisibility: MutableLiveData<Boolean> = MutableLiveData()

    private val moviesDataSourceFactory =
        MoviesDataSourceFactory(
            moviesPaginatedUseCase,
            loadingVisibility,
            noConnectionLiveData,
            compositeDisposable,
            retryManager
        )

    init {
        moviesPaginatedUseCase.addDisposable(compositeDisposable)

        val config = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setEnablePlaceholders(true)
            .build()

        moviesLiveData = LivePagedListBuilder(moviesDataSourceFactory, config).build()

        //compositeDisposable +=
        /*moviesListPaginatedRepository.totalPages()
            .subscribe { totalPages
            -> totalPagesLiveData.postValue(totalPages)
        }*/
    }

    fun refresh() {
        moviesDataSourceFactory.pageMovieDataSource?.invalidate()
    }

    override fun onCleared() {
        retryManager.clear()
        moviesPaginatedUseCase.dispose()
        super.onCleared()
    }
}