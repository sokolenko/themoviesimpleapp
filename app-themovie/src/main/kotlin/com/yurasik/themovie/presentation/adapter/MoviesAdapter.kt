package com.yurasik.themovie.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.yurasik.app_core.livedata.SingleLiveEvent
import com.yurasik.domain.model.movie.Movie
import com.yurasik.themovie.presentation.adapter._base.LifecycleRecyclerPagedListAdapter
import com.yurasik.themovie.presentation.adapter.viewholders.BaseMovieViewHolder
import com.yurasik.themovie.presentation.adapter.viewholders.MovieViewHolder
import com.yurasik.themovie.presentation.adapter.viewholders.ProgressViewHolder

class MoviesAdapter(val moviesSelectionDelegate: SingleLiveEvent<Movie>) :
    LifecycleRecyclerPagedListAdapter<Movie, BaseMovieViewHolder>(
        diffCallback
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when(viewType) {
            VIEW_TYPE_NORMAL -> MovieViewHolder(
                parent,
                moviesSelectionDelegate
            )
            else -> ProgressViewHolder(
                parent
            )
        }

    override fun onBindViewHolder(holder: BaseMovieViewHolder, position: Int) {
        holder.bindTo(position, getItem(position))
    }

    override fun getItem(position: Int): Movie? {
        if (position == super.getItemCount()) return null
        return super.getItem(position)
    }

    override fun getItemCount(): Int {
        return super.getItemCount()+1
    }

    override fun getItemViewType(position: Int) =
        if (position == super.getItemCount()) {
            VIEW_TYPE_LOADING
        } else {
            VIEW_TYPE_NORMAL
        }

    companion object {

        private const val VIEW_TYPE_NORMAL = 0
        private const val VIEW_TYPE_LOADING = 1

        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         * <p>
         * When you add a Cheese with the 'Add' button, the PagedListAdapter uses diffCallback to
         * detect there's only a single item difference from before, so it only needs to animate and
         * rebind a single view.
         *
         * @see DiffUtil
         */
        private val diffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                oldItem.id == newItem.id

            /**
             * Note that in kotlin, == checking on data classes compares all contents, but in Java,
             * typically you'll implement Object#equals, and use it to compare object contents.
             */
            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                oldItem == newItem
        }
    }
}
