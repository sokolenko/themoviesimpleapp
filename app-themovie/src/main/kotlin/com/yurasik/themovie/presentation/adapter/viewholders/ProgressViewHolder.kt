package com.yurasik.themovie.presentation.adapter.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import yurasik.com.themovie.R

class ProgressViewHolder(parent: ViewGroup) : BaseMovieViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.item_progress, parent, false))