package com.yurasik.themovie.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import yurasik.com.themovie.R
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState==null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.flContainer, MovieFragment())
                .commitAllowingStateLoss()
        }
    }

    override fun androidInjector() = dispatchingAndroidInjector

}
