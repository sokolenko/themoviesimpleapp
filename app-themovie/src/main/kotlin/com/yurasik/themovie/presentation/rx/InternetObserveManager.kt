package com.yurasik.themovie.presentation.rx

import android.content.Context
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.yurasik.domain.util.rx.retry.RetryEvent
import com.yurasik.domain.util.rx.retry.RetryManager
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class InternetObserveManager
    @Inject constructor(
    context: Context
) : RetryManager {

    private val observableNetwork: Observable<Connectivity> = ReactiveNetwork
        .observeNetworkConnectivity(context)

    private val disposable = CompositeDisposable()

    private val connectivitySubject = BehaviorSubject.create<Boolean>()

    init {
        disposable.add(observableNetwork
            .observeOn(Schedulers.io())
            .doOnNext {
                connectivitySubject.onNext(it.available())
            }
            .subscribe())
    }

    override fun observeRetries(error: Throwable): Flowable<RetryEvent> {
        return connectivitySubject
            .filter { it }
            .toFlowable(BackpressureStrategy.DROP)
            .flatMap{
                    Flowable.just(RetryEvent())
            }
    }

    override fun clear() {
        disposable.clear()
    }
}