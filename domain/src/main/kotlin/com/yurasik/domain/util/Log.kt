package com.yurasik.domain.util

class Log {
    companion object {
        fun d(tag: String? = "TAG", message: String? = null){
            println("$tag, ${message ?: ""}")
        }
        fun e(tag: String? = "TAG", message: String? = null){
            println("$tag, ${message ?: ""}")
        }
    }
}