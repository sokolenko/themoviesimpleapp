package com.yurasik.domain.util.rx.switcher

import io.reactivex.Observable
import io.reactivex.Observer

interface ISwitcher {
    val switcherObserver: Observer<Boolean>
    val switcherObservable: Observable<Boolean>
}