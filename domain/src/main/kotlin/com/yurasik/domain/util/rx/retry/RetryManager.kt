package com.yurasik.domain.util.rx.retry

import io.reactivex.Flowable

interface RetryManager {
    fun observeRetries(error: Throwable): Flowable<RetryEvent>
    fun clear()
}