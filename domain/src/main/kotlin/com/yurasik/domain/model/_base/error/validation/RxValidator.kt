package com.yurasik.domain.model._base.error.validation

import com.yurasik.domain.repositories.IResourcesRepository
import io.reactivex.Single

abstract class RxValidator<T>(val bundleResourcesRepository: IResourcesRepository? = null) {

    fun validate(model: T): Single<T> = Single.create { emitter ->
        val validationErrors = getValidationClauses(model).mapNotNull { clause -> clause.errorIfInvalid() }

        if (validationErrors.isEmpty()) {
            emitter.onSuccess(model)
        } else {
            emitter.onError(ValidationException(validationErrors))
        }
    }

    abstract fun getValidationClauses(model: T): List<ValidationClause>

    interface ValidationClause {
        fun errorIfInvalid(): ValidationError?
    }
}

