package com.yurasik.domain.model._base

sealed class ProcessStatus

object Idle : ProcessStatus()
object Processing : ProcessStatus()
