package com.yurasik.domain.model._base.error.validation

import com.yurasik.domain.exceptions.TheException

open class ValidationException(val validationErrors: List<ValidationError>) : TheException() {

    override fun toString(): String {
        return validationErrors.joinToString("\n") { it.getErrorMessage() }
    }
}

class SimpleValidationException(error: String) : ValidationException(listOf(ValidationError(error)))