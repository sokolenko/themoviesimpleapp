package com.yurasik.domain.model.movie

data class Movie (
    var id: Int? = null,
    var popularity: Double? = null,
    var voteCount: Int? = null,
    var video: Boolean? = null,
    var posterPath: String? = null,
    var adult: Boolean? = null,
    var backdropPath: String? = null,
    var originalLanguage: String? = null,
    var originalTitle: String? = null,
    var genreIds: List<Int>? = null,
    var title: String? = null,
    var voteAverage: Double? = null,
    var overview: String? = null,
    var releaseDate: String? = null
)