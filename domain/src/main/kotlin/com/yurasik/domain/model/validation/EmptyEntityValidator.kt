package com.yurasik.domain.model.validation

import com.yurasik.domain.model._base.error.validation.RxValidator
import com.yurasik.domain.model._base.error.validation.ValidationError
import com.yurasik.domain.repositories.IResourcesRepository

class EmptyEntityValidator(resourcesRepository: IResourcesRepository) : RxValidator<String>(resourcesRepository) {
    override fun getValidationClauses(model: String): List<ValidationClause> {
        return listOf(EmptyEntityValidatonClause(model))
    }
}

class EmptyEntityValidatonClause(val text: String) : RxValidator.ValidationClause {

    override fun errorIfInvalid(): ValidationError? {
        if (text.trim().isEmpty()) {
            return ValidationError("Empty field!")
        }
        return null
    }
}