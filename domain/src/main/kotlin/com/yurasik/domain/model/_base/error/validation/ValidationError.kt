package com.yurasik.domain.model._base.error.validation

import com.yurasik.domain.model._base.error.TheError

open class ValidationError(private val error: String,
                           private val field: String? = null) : TheError() {
    // Override if required
    open fun getField() = field
    open fun getErrorMessage() = error
}