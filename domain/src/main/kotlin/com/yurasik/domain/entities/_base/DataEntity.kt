package com.yurasik.domain.entities._base


/**
 * A generic wrapper class around data request
 */

sealed class ResultEntity<out RequestData>{

    class SUCCESS<RequestData>(var data: RequestData? = null): ResultEntity<RequestData>()

    class LOADING<RequestData>(var data: RequestData? = null): ResultEntity<RequestData>()

    class ERROR<RequestData>(var error: ErrorEntity, var data: RequestData? = null): ResultEntity<RequestData>()

    class Empty
}