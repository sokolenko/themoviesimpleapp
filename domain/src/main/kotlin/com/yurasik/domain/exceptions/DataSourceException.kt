package com.yurasik.domain.exceptions

open class DataSourceException(val errorMessage: String) : TheException() {
    override fun toString(): String {
        return "${super.toString()}\nerrorMessage = $errorMessage"
    }
}