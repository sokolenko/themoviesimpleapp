package com.yurasik.domain.repositories

interface IResourcesRepository {
    fun getString(resId: Int): String
    fun getString(resId: Int, vararg args: Any): String
    fun getStringArray(resId: Int): Array<String>
}