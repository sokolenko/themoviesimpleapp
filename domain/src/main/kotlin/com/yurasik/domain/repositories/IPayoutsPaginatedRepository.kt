package com.yurasik.domain.repositories

import com.yurasik.domain.model.movie.Movie
import io.reactivex.Single

interface IMoviesPaginatedRepository {
    fun totalPages(): Single<Int>
    fun getMovies(page: Int, perPage: Int): Single<List<Movie>>
}