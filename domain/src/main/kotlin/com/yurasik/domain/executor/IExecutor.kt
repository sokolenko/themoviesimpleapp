package com.yurasik.domain.executor

import io.reactivex.Scheduler

interface IExecutor {
    val scheduler: Scheduler
}