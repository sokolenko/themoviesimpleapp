package com.yurasik.domain.usecase.movies

import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.repositories.IMoviesPaginatedRepository
import com.yurasik.domain.usecase._base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class MoviesUseCase @Inject constructor(
    private val repository: IMoviesPaginatedRepository
) : SingleUseCase<List<Movie>, MoviesUseCase.Params>(){

    override fun buildUseCaseSingle(params: Params?): Single<List<Movie>> {
        return repository.getMovies(params!!.page, 10)
    }

    class Params(val page: Int)
}
