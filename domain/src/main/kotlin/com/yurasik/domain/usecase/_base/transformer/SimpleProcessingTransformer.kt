package com.yurasik.domain.usecase._base.transformer

import com.yurasik.domain.entities._base.NetworkState
import com.yurasik.domain.usecase._base.events.IMutableLiveData
import io.reactivex.Completable
import io.reactivex.CompletableSource
import io.reactivex.CompletableTransformer

class SimpleProcessingCompletableTransformer(private val processingLiveData: IMutableLiveData<NetworkState>) : CompletableTransformer {

    override fun apply(upstream: Completable): CompletableSource {
        return upstream.doOnSubscribe { processingLiveData.postValue(NetworkState.LOADED) }
            .doFinally { processingLiveData.postValue(NetworkState.LOADING) }
    }

}