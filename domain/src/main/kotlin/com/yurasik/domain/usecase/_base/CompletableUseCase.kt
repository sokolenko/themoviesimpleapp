package com.yurasik.domain.usecase._base

import com.yurasik.domain.usecase._base.observer.SimpleDisposableCompletableObserver
import io.reactivex.Completable
import io.reactivex.CompletableTransformer
import io.reactivex.observers.DisposableCompletableObserver

abstract class CompletableUseCase<in Params> : BaseReactiveUseCase() {

    abstract fun buildUseCaseCompletable(params: Params? = null): Completable

    fun execute(observer: DisposableCompletableObserver = object : SimpleDisposableCompletableObserver() {}, params: Params? = null) = execute(observer, null, params)

    fun execute(observer: DisposableCompletableObserver, transformer: CompletableTransformer? = null, params: Params? = null) {
        var completable = buildUseCaseCompletable(params)
        if (transformer != null) {
            completable = completable.compose(transformer)
        }
        addDisposable(completable.subscribeWith(observer))
    }
}