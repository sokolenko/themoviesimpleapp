package com.yurasik.domain.usecase.movies

import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.repositories.IMoviesPaginatedRepository
import com.yurasik.domain.usecase._base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class MoviesPaginatedUseCase @Inject constructor(
    private val repository: IMoviesPaginatedRepository
) : SingleUseCase<List<Movie>, MoviesPaginatedUseCase.Params>(){

    override fun buildUseCaseSingle(params: Params?): Single<List<Movie>> {
        return repository.getMovies(params!!.page, params.perPage)
    }

    class Params(val page: Int, val perPage: Int)
}
