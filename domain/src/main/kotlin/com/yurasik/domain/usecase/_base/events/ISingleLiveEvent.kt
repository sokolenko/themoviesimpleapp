package com.yurasik.domain.usecase._base.events

interface ISingleLiveEvent<T> {
    fun call()
    fun postValue(t: T?) {
    }
}
