package com.yurasik.domain.usecase._base.observer

import com.yurasik.domain.exceptions.DataSourceException
import com.yurasik.domain.usecase._base.events.ISingleLiveEvent
import io.reactivex.observers.DisposableCompletableObserver

abstract class SimpleDisposableCompletableObserver() : DisposableCompletableObserver() {

    var commonErrorEvent: ISingleLiveEvent<String>? = null

    constructor(showErrorEvent: ISingleLiveEvent<String>) : this() {
        this.commonErrorEvent = showErrorEvent
    }

    override fun onComplete() {
        // Override to handle result
    }

    override fun onError(e: Throwable) {
        e.printStackTrace()

        when (e) {
            is DataSourceException -> commonErrorEvent?.postValue(e.errorMessage)
            is IllegalStateException -> commonErrorEvent?.postValue(e.message ?: "unknown error")
        }
    }
}