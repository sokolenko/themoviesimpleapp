package com.yurasik.domain.usecase._base.events

interface IMutableLiveData<T> {
    fun postValue(value: T)
}