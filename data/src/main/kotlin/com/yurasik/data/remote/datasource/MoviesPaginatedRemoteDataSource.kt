package com.yurasik.data.remote.datasource

import com.yurasik.domain.common.Mapper
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject
import com.yurasik.data.datasource.movies.IMoviesPaginatedDataSource
import com.yurasik.data.remote.retrofit.movie.api.MoviesApiService
import com.yurasik.data.remote.retrofit.movie.model.MovieRemote
import com.yurasik.domain.executor.IExecutor
import com.yurasik.domain.model.movie.Movie
import yurasik.com.data.BuildConfig
import javax.inject.Inject

class MoviesPaginatedRemoteDataSource @Inject constructor(
    private val remoteExecutor: IExecutor,
    private val mapper: Mapper<MovieRemote, Movie>,
    private val moviesApiService: MoviesApiService
): IMoviesPaginatedDataSource {

    private val totalPagesSubject = SingleSubject.create<Int>()

    override fun getTotalPages() = totalPagesSubject

    override fun getMovies(page: Int): Single<List<Movie>> {
        return moviesApiService.getPopularMovies(page = page)
            .subscribeOn(remoteExecutor.scheduler)
            .map {
                totalPagesSubject.onSuccess(it.totalPages)
                it.results.map { mapper.mapFrom(it) }
            }
            .flatMap {
                if (BuildConfig.DEBUG) {
                    Thread.sleep(1000)
                }
                Single.just(it)
            }
    }

}