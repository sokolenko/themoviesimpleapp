package com.yurasik.data.remote.retrofit

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.yurasik.data.remote.retrofit.interceptor.AccessKeyPathInterceptor
import java.util.concurrent.TimeUnit

fun createOkHttpClient(
    authenticator: AccessKeyPathInterceptor,
    isDebug: Boolean = true
): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.HEADERS }

    return OkHttpClient.Builder().apply {
        connectTimeout(30, TimeUnit.SECONDS)
        readTimeout(30, TimeUnit.SECONDS)
        addInterceptor(authenticator)
        if (isDebug) {
            addInterceptor(httpLoggingInterceptor)
        }
    }.build()
}

fun createRetrofit(
    okHttpClient: OkHttpClient,
    url: String,
    factory: Converter.Factory = GsonConverterFactory.create(Gson()) ) = Retrofit.Builder()
    .baseUrl(url)
    .client(okHttpClient)
    .addConverterFactory(factory)
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build()
