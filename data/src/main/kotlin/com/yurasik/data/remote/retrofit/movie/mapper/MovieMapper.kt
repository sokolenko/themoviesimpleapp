package com.yurasik.data.remote.retrofit.movie.mapper

import com.yurasik.domain.common.Mapper
import com.yurasik.data.remote.retrofit.movie.model.MovieRemote
import com.yurasik.domain.model.movie.Movie

class MovieMapper : Mapper<MovieRemote, Movie>() {
    override fun mapFrom(from: MovieRemote): Movie {
        return Movie(
                    id = from.id,
                    title = from.title,
                    releaseDate = from.releaseDate,
                    overview = from.overview,
                    backdropPath = from.backdropPath
                )
    }
}