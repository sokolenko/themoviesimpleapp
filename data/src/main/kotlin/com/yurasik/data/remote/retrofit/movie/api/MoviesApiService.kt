package com.yurasik.data.remote.retrofit.movie.api

import io.reactivex.Single
import org.jetbrains.annotations.Nullable
import retrofit2.http.GET
import retrofit2.http.Query
import com.yurasik.data.remote.retrofit.movie.model.MovieRemoteResponse

interface MoviesApiService {

    @GET("/3/movie/popular")
    fun getPopularMovies(
        @Query("language")
        language: String? = "en",
        @Query("page")
        page: Int,
        @Nullable @Query("region")
        region: String? = null
    ): Single<MovieRemoteResponse>

}