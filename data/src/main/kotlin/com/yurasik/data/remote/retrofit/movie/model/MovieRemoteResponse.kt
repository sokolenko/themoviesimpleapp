package com.yurasik.data.remote.retrofit.movie.model

import com.google.gson.annotations.SerializedName

data class MovieRemoteResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("results")
    val results: List<MovieRemote>
)

