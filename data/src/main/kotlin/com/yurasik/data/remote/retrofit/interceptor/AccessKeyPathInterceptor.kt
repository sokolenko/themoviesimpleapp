package com.yurasik.data.remote.retrofit.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AccessKeyPathInterceptor(): Interceptor {

    private val apiKey = "api_key=df6e5436a580887a24be789091b3dec5"

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val newRequestBuilder = request.newBuilder()

        val requiresAuthorization = !request.url.toString().contains(apiKey)
        if (requiresAuthorization) {
            newRequestBuilder
                .url(request.url.toString().plus("&").plus(apiKey))
                .build()
        }
        return chain.proceed(newRequestBuilder.build())
    }
}