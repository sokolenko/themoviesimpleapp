package com.yurasik.data.datasource._base

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

interface ISimpleKeyedDataSource<Key, Data> {

    fun getData(key: Key): Maybe<Data>
    fun observeData(key: Key): Observable<Data>

    fun setData(key: Key, data: Data): Completable

    fun isValid(key: Key): Boolean
}