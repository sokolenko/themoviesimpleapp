package com.yurasik.data.datasource._base

import io.reactivex.Completable
import io.reactivex.Single

interface IValidatable {

    fun isValid(): Single<Boolean>
    fun invalidate(): Completable
}