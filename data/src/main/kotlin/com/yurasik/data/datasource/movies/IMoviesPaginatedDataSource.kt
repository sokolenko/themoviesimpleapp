package com.yurasik.data.datasource.movies

import com.yurasik.domain.model.movie.Movie
import io.reactivex.Single

interface IMoviesPaginatedDataSource {

    fun getTotalPages(): Single<Int>

    fun getMovies(
        page: Int
    ): Single<List<Movie>>
}