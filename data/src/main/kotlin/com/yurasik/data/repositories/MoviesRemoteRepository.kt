package com.yurasik.data.repositories

import io.reactivex.Single
import com.yurasik.data.datasource.movies.IMoviesPaginatedDataSource
import com.yurasik.domain.model.movie.Movie
import com.yurasik.domain.repositories.IMoviesPaginatedRepository
import javax.inject.Inject

class MoviesRemoteRepository @Inject constructor(
    private val dataSource: IMoviesPaginatedDataSource
)
    : IMoviesPaginatedRepository {
    override fun totalPages() = dataSource.getTotalPages()

    override fun getMovies(page: Int, perPage: Int): Single<List<Movie>> {
        return dataSource.getMovies(page)
    }
}