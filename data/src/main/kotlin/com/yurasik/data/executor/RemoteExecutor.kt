package com.yurasik.data.executor

import com.yurasik.domain.executor.IExecutor
import io.reactivex.schedulers.Schedulers

class RemoteExecutor : IExecutor {
    override val scheduler = Schedulers.io()
}